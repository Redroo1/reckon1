﻿using NUnit.Framework;

namespace Test2.test
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void SubstringWithMixCases()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "Peter");
            Assert.That(expectedResult, Is.EqualTo("1, 43, 98"));
        }

        [Test]
        public void SubstringWithLowerCase()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "peter");
            Assert.That(expectedResult, Is.EqualTo("1, 43, 98"));
        }

        [Test]
        public void SubstringWithUpperCase()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "PETER");
            Assert.That(expectedResult, Is.EqualTo("1, 43, 98"));
        }

        [Test]
        public void SubstringWithPartOfWord()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "Pick");
            Assert.That(expectedResult, Is.EqualTo("53, 81"));
        }

        [Test]
        public void SubstringWithPartOfMultipleWords()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "Pick");
            Assert.That(expectedResult, Is.EqualTo("53, 81"));
        }

        [Test]
        public void SubstringWithNotExistString()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out.Phew!", "Z");
            Assert.That(expectedResult, Is.EqualTo("<No Output>"));
        }

        [Test]
        public void SubstringWithOverlappedString()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Pipipi", "Pipi");
            Assert.That(expectedResult, Is.EqualTo("1, 3"));
        }

        [Test]
        public void SubstringWithLongerSubString()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Pipi", "Pipipi");
            Assert.That(expectedResult, Is.EqualTo("<No Output>"));
        }

        [Test]
        public void SubstringWithAlmostMatchedSubString()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Pipi", "Pit");
            Assert.That(expectedResult, Is.EqualTo("<No Output>"));
        }

        [Test]
        public void SubstringWithMatchedAllChars()
        {
            FindSubString fs = new FindSubString();
            string expectedResult = fs.Find(@"Aaaa", "a");
            Assert.That(expectedResult, Is.EqualTo("1, 2, 3, 4"));
        }

    }
}
